<?php
/*
Template Name: About
 */
 require_once get_template_directory() . '/components/header.php';
 require_once get_template_directory() . '/components/navigation.php';

?>
    <div class="about-bg">
        <div class="container-fluid">
            <h1 class="page-heading">About Us</h1>
            <p class="about-headline text-center">Who's behind Voluum?</p>
            <div class="row about-info-wrap">
              <div class="col-md-6 col-sm-12 text-center">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/logo.png" alt="" class="logo-img mt-4">
              </div>
              <div class="col-md-6 col-sm-12">
                <p>Codewise is the leading provider of ad exchange and ad measurement & optimization
                  software platforms, recognized by the Financial Times among the fastest-growing
                  companies in 2017 and 2018. Entirely self-funded and profitable, Codewise employs
                  200 industry best pros across Poland, USA and UK and serves more than 5000 customers
                  in over 190 countries.</p>
              </div>
            </div> <!-- /row -->
            <div class="row">
              <div class="col-lg-3 col-md-6 col-sm-10 team-column mx-auto">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/robert.jpg" alt="" class="about-profile-img">
                <div class="about-column-inner">
                  <p class="team-name text-center">Robert Gryn</p>
                  <p class="team-position text-center">CEO and Owner</p>
                  <p class="team-bio">Robert defines the overall strategy of Codewise. Robert turned Codewise into one of Europe’s fastest-growing companies, with a 290% average revenue growth rate since the beginning of operations in 2012. His vision empowered the creation of Voluum, the industry-standard ads analytics and optimization SaaS adopted by thousands of digital and performance marketers. Robert holds a BSc in Technology Entrepreneurship and is a member of the Forbes Technology Council.</p>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-10 team-column mx-auto">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/john.jpg" alt="" class="about-profile-img">
                <div class="about-column-inner">
                  <p class="team-name text-center">Dr. John Malatesta</p>
                  <p class="team-position text-center">President, CRO, CMO</p>
                  <p class="team-bio">Robert defines the overall strategy of Codewise. Robert turned Codewise into one of Europe’s fastest-growing companies, with a 290% average revenue growth rate since the beginning of operations in 2012. His vision empowered the creation of Voluum, the industry-standard ads analytics and optimization SaaS adopted by thousands of digital and performance marketers. Robert holds a BSc in Technology Entrepreneurship and is a member of the Forbes Technology Council.</p>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-10 team-column mx-auto">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/piotr.jpg" alt="" class="about-profile-img">
                <div class="about-column-inner">
                  <p class="team-name text-center">Piotr Malecki</p>
                  <p class="team-position text-center">CFO</p>
                  <p class="team-bio">Robert defines the overall strategy of Codewise. Robert turned Codewise into one of Europe’s fastest-growing companies, with a 290% average revenue growth rate since the beginning of operations in 2012. His vision empowered the creation of Voluum, the industry-standard ads analytics and optimization SaaS adopted by thousands of digital and performance marketers. Robert holds a BSc in Technology Entrepreneurship and is a member of the Forbes Technology Council.</p>
                </div>
              </div>
              <div class="col-lg-3 col-md-6 col-sm-10 team-column mx-auto">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/maciej.jpg" alt="" class="about-profile-img">
                <div class="about-column-inner">
                  <p class="team-name text-center">Maciej Szlachta</p>
                  <p class="team-position text-center">CTO</p>
                  <p class="team-bio">Robert defines the overall strategy of Codewise. Robert turned Codewise into one of Europe’s fastest-growing companies, with a 290% average revenue growth rate since the beginning of operations in 2012. His vision empowered the creation of Voluum, the industry-standard ads analytics and optimization SaaS adopted by thousands of digital and performance marketers. Robert holds a BSc in Technology Entrepreneurship and is a member of the Forbes Technology Council.</p>
                </div>
              </div>
            </div>
            <div class="row company-numbers">
              <div class="col-md-4 col-sm-12 text-center">
                <div class="numbers-wrap">
                  <span class="company-numbers-left">$2.5 bn</span>
                  <span class="company-numbers-right">ads revenue tracked annually</span>
                </div>
                <div class="company-numbers-separator mx-auto"></div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="numbers-wrap">
                  <span class="company-numbers-left">200</span>
                  <span class="company-numbers-right">employees in offices in the USA, UK, and Poland<span>
                </div>
                <div class="company-numbers-separator mx-auto"></div>
              </div>
              <div class="col-md-4 col-sm-12">
                <div class="numbers-wrap">
                  <span class="company-numbers-left">5000</span>
                  <span class="company-numbers-right">customers all around the world</span>
                </div>
              </div>
            </div>
            <div class="contact-wrap text-center">
              <a class="button" href="#">Contact Us ></a>
            </div>
            <div class="we-support">
              <h4 class="we-support-title">Proud Member of:</h4>
              <div class="row">
                <div class="col-md-12 col-lg-6 mb-3">
                  <div class="row">
                    <div class="col-sm-12 col-md-6 we-support-box support-left">
                      <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/wef-color.png" alt="">
                      <p class="we-support-text">Forum Member</p>
                    </div>
                    <div class="col-sm-12 col-md-6 we-support-box">
                      <div class="row mb-4">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ai-color.png" alt="" class="support-img">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/ec-color.png" alt="" class="support-img">
                      </div>
                      <a class="read-more">read more</a>
                    </div> <!-- /col -->
                  </div> <!-- /row -->
                </div> <!-- /col -->
                <div class="col-md-12 col-lg-6 mb-4">
                  <div class="row">
                    <div class="col-sm-12 col-md-6 we-support-box support-left">
                      <div class="go">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/eif-color.png" alt="" class="support-img support-img-resize">
                        <a class="read-more">read more</a>
                      </div>
                    </div>
                    <div class="col-sm-12 col-md-6 we-support-box support-bottom">
                      <div class="row mb-4">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/aod-color.png" alt="" class="support-img">
                      </div>
                      <a class="read-more">read more</a>
                    </div>
                  </div> <!-- /row -->
                </div> <!-- /col -->
              </div> <!-- /row -->
            </div> <!-- /we-support -->
            <div class="we-support">
              <h4 class="we-support-title">Recognition:</h4>
            </div>
            <div class="row icon-row">
                <div class="col-6 col-sm-3">
                    <div class="icon float-left">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/golfer_icon.png" alt="">
                    </div>
                    <h3>Instruction</h3>
                    <p>Our secret to becoming a better golfer.</p>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="icon float-left">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/golfball_icon.png" alt="">
                    </div>
                    <h3>Products</h3>
                    <p>Top accessories from industry leading brands.</p>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="icon float-left">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/club_icon.png" alt="">
                    </div>
                    <h3>Equipment</h3>
                    <p>The best clubs from leading manufacturers.</p>
                </div>
                <div class="col-6 col-sm-3">
                    <div class="icon float-left">
                        <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/membership_icon.png" alt="">
                    </div>
                    <h3>Perks</h3>
                    <p>Members receive discounts and free shipping.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid about-container">
        <div class="row">
            <div class="col-12 col-sm-6">
                <h1>Our Company</h1>
                <p>We're a team of writers, creatives, developers and customer loyalty specialists. Most importantly, we're all golfers! We developed the Swingbits golf system and our store to help golfers of all skill levels improve their games and have the gear to match without breaking the bank.</p>
                <p>We're a team of writers, creatives, developers and customer loyalty specialists. Most importantly, we're all golfers! We developed the Swingbits golf system and our store to help golfers of all skill levels improve their games and have the gear to match without breaking the bank.</p>
            </div>
            <div class="col-12 col-sm-6">
                <img src="<?php echo get_bloginfo( 'template_directory' ); ?>/img/office.png" alt="">
            </div>
        </div>
    </div>


    <?php require_once get_template_directory() . '/components/footer.php'; ?>
